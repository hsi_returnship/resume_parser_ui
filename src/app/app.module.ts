import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule} from '@angular/router';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { AppComponent } from './app.component';
//import {LoginComponent} from'./login/login.component';
import { AppRoutes } from './app.routing';

import {FileSelectDirective} from "ng2-file-upload";
//import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
//import { MenuItem } from 'primeng/api';                 //api
//import {TableModule} from 'primeng/table';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    AppComponent    
  //  LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(AppRoutes),
    Ng2SearchPipeModule,
    AgGridModule.withComponents([])
    //TableModule,  
    //AccordionModule,    
    //MenuItem
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
