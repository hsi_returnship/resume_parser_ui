export class CandidateDataModel {

    public candidateName: string;
    public candidateEmail: string;
    public candidatePhone: string;
    public skills: string;
    public qualification: string;
    public experience: string;
    public uploadedFileName: string;
    public uploadedDate: Date;    
}