import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { ResumeService } from './resume.service';
import { FilterPipe } from './filter.pipe';
//import { CandidateNamePipe } from "./CandidateNamePipe";
import { FileLikeObject, FileUploader } from 'ng2-file-upload';
import { ConnectableObservable, Observable } from 'rxjs';
import { CandidateDataModel } from './CandidateDataModel';


@Component({
  selector: 'app-resumeupload',
  templateUrl: './resumeupload.component.html',
  styleUrls: ['./resumeupload.component.scss']
})
export class ResumeuploadComponent implements OnInit {

  @Input() maxFiles: number = 5;
  @Input() fileExt: string = "DOC,DOCX,PDF";
  @Input() maxSize: number = 25;

  rowData: any[] = [];
  errorMessage: any;
  searchText: any;
  searchName: any;
  public show: boolean = false;
  public uploadStatus: boolean = false;
  public convertStatus: boolean = true;
  public parseBtnStatus: boolean=false;
  public proceedBtnStatus: boolean=false;
   
  public selectedFiles;
    fileToUpload: File[]=[];
	
// code for export file json to excel
  public downloadData:CandidateDataModel;
  private downloadDatas: CandidateDataModel[]=[];
  
  public errors: Array<string> = [];
  //public uploadFilesUrl: string = "";
  //public uploadParseUrl: string = "";
  //paths = [];

  //public uploader: FileUploader = new FileUploader({});
  //public hasBaseDropZoneOver: boolean = false;
  message: string = '';
  
  

  constructor(private resumeService: ResumeService) { }

  ngOnInit() {

    this.selectedFiles="Choose File";
    
  }

  //file upload doc ,docx,pdf  event handling

  handleFileInput(event) {

    let files: FileList = event.target.files;
    console.log("Inside handle file>>>>" + files.length);
    if ((files.length > 0)) {
      this.selectedFiles="";
      for (let i=0;i<files.length;i++)
      {
        this.fileToUpload.push(files.item(i));
        this.selectedFiles+="\""+files.item(i).name+"\""
      }      
    }
    console.log(this.fileToUpload);
    this.proceedBtnStatus=true;
  }
  public proceed()
  {
    
    this.resumeService.uploadFileService(this.fileToUpload)
      .subscribe(
        success => {
          this.message = success;
          alert (this.message);
          // this.uploadFiles = null;
          // this.myInputVariable.nativeElement.value = "";
        },
        error => { this.errorMessage = <any>error; }
      );
      this.parseBtnStatus=true;
  }
  
  columnDefs = [
    {headerName: 'Name', field: 'candidateName',width:200, sortable: true, filter: true },
    {
      headerName: 'Skills', field: 'skills',width:300, sortable: true, resizable: true,
      filter: "agTextColumnFilter",
      filterParams: { defaultOption: "startsWith" }
 
    },
    {headerName: 'Qualification', field: 'qualification',width:200, sortable: true, filter: true, resizable: true},
    {headerName: 'Experience', field: 'experience',width:100, sortable: true, filter: true},
    {headerName: 'Email', field: 'candidateEmail',width:200, sortable: true, filter: true },
    {headerName: 'Mobile', field: 'candidatePhone',width:150, sortable: true, filter: true},    
    {headerName: 'Uploaded Date', field: 'uploadedDate',width:170, sortable: true, filter: true},
    {headerName: 'File Name', field: 'uploadedFileName',width:200, sortable: true, filter: true}
];
  
style = {
  marginTop: '20px',
  width: '100%',
  height: '100%',
  boxSizing: 'border-box'
};

  public parseResume() {
    this.show=false;
    this.resumeService.sendParsePathUrl()
      .subscribe(
        success => {
          this.rowData = success;
          console.log(this.rowData);          
        },
        error => { this.errorMessage = <any>error; }
      );
      this.show = !this.show;
  }
 
 // Download Document 

  downloadDocument(){
	for(var i=0; i<this.rowData.length;i++){
      this.downloadData= new CandidateDataModel();
      this.downloadData.candidateName = this.rowData[i].candidateName;
      this.downloadData.candidateEmail = this.rowData[i].candidateEmail;
      this.downloadData.candidatePhone = this.rowData[i].candidatePhone;
      this.downloadData.skills = this.rowData[i].skills;
      this.downloadData.qualification = this.rowData[i].qualification;
      this.downloadData.experience = this.rowData[i].experience;
      this.downloadData.uploadedFileName = this.rowData[i].uploadedFileName;
      this.downloadData.uploadedDate = this.rowData[i].uploadedDate;
      this.downloadDatas.push(this.downloadData);
    }
        var csvData = this.ConvertToCSV(this.downloadDatas);
		this.downloadDatas=[];
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], { type: 'text/csv' });
        var url= window.URL.createObjectURL(blob);
        a.href = url;
        a.download = 'candidateDetailsList.csv';
        a.click();
}

ConvertToCSV(objArray) {
  var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
  var str = "";
  var row = "";

  for (var index in objArray[0]) {
      //Now convert each value to string and comma-separated
      row += index + ',';
  }
  row = row.slice(0, -1);
  //append Label row with line break
  str += row + '\r\n';

  for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
          if (line != '') line += ','

          line += array[i][index];
      }
      str += line + '\r\n';
  }
  return str;
}
 
 public clearPage(){
    this.show=false;
  this.rowData = [];
  this.fileToUpload=[]
  //this.selectedFiles="Choose File";
  this.proceedBtnStatus=false;
  this.parseBtnStatus=false;
  this.downloadDatas=[];
  this.ngOnInit();
  }

}
